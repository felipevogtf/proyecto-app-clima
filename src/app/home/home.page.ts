import { ViewChild, Component, OnInit, Renderer2, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { NavController, IonRow, IonSlides } from '@ionic/angular';
import { ApiclimaService } from '../services/apiclima.service';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChildren(IonRow, { read: ElementRef }) rows: QueryList<any>;
  @ViewChild(IonSlides) slides: IonSlides;

  public pronostico: any;
  public activeRow: any;


  constructor(
    public navCtrl: NavController,
    public proveedor: ApiclimaService,
    public geolocation: Geolocation,
    private renderer: Renderer2,
    private el: ElementRef) { }


  ngOnInit() {
    this.geolocationNative();
  }

  ngAfterViewInit() {
    this.setDefaultSize();
    this.selectRow(this.selectTemperatureRange(this.parseTemperature(this.pronostico.currently.temperature)));
  }

  geolocationNative() {
    this.geolocation.getCurrentPosition().then((geoposition: Geoposition) => {
      this.proveedor.getForecast(geoposition.coords.latitude, geoposition.coords.longitude).subscribe(week => {
        this.pronostico = week;
        console.log(this.pronostico);
      },
      error => console.error(error)
      )
    }
    )
  }

  setDefaultSize() {
    for (var i = 0; i < 18; i++) {
      this.renderer.removeClass(this.rows.toArray()[i].nativeElement, "preload");
      this.renderer.addClass(this.rows.toArray()[i].nativeElement, "defaultSize");
    }
  }

  selectRow(number) {
    this.activeRow = this.rows.toArray()[number].nativeElement;
    this.renderer.removeClass(this.activeRow, "defaultSize");
    this.renderer.addClass(this.activeRow, "activeRow");
    this.addContent(this.activeRow);
  }

  addContent(row) {

    //Firs row
    const time = this.renderer.createElement('ion-col');
    const value = this.renderer.createText(this.parseTemperature(this.pronostico.currently.temperature) + "°");

    this.renderer.addClass(time, "tiempo");
    this.renderer.setAttribute(time, "size", "6");
    this.renderer.appendChild(time, value);

    const icon = this.renderer.createElement('ion-col');
    const rute = this.renderer.createElement('ion-img');

    this.renderer.setAttribute(icon, "size", "6");
    this.renderer.setAttribute(rute, "src", "assets/icon/Darksky/" + this.pronostico.currently.icon + ".png");
    this.renderer.addClass(rute, "icono");
    this.renderer.appendChild(icon, rute);
    //End first row

    //Second row
    //Prob lluvia.
    const precipProbability = this.renderer.createElement('ion-col');
    const valuePre = this.renderer.createText((this.pronostico.currently.precipProbability * 100)+'');
    
    this.renderer.addClass(precipProbability, "detalles");
    this.renderer.setAttribute(precipProbability, "size", "2");
    this.renderer.appendChild(precipProbability, valuePre);

    const iconPrecip = this.renderer.createElement('ion-col');
    const rutePrecip = this.renderer.createElement('ion-img');

    this.renderer.setAttribute(iconPrecip, "size", "1");
    this.renderer.setAttribute(rutePrecip, "src", "assets/icon/detalles/water.png");
    this.renderer.addClass(rutePrecip, "iconDetails");
    this.renderer.appendChild(iconPrecip, rutePrecip);

    const porcentaje = this.renderer.createText('%');
    const divPorcentaje = this.renderer.createElement('ion-col');
    this.renderer.addClass(divPorcentaje, "Si");
    this.renderer.setAttribute(divPorcentaje, "size", "1");
    this.renderer.appendChild(divPorcentaje,porcentaje);
    //End prob. lluvia
    //Humedad
    const humidity = this.renderer.createElement('ion-col');
    const valueHum = this.renderer.createText((this.pronostico.currently.humidity * 100) + '');
    
    this.renderer.addClass(humidity, "detalles");
    this.renderer.setAttribute(humidity, "size", "2");
    this.renderer.appendChild(humidity, valueHum);

    const iconHumidity= this.renderer.createElement('ion-col');
    const ruteHumidity = this.renderer.createElement('ion-img');

    this.renderer.setAttribute(iconHumidity, "size", "1");
    this.renderer.setAttribute(ruteHumidity, "src", "assets/icon/detalles/humidity.png");
    this.renderer.addClass(ruteHumidity, "iconDetails");
    this.renderer.appendChild(iconHumidity, ruteHumidity);

    const porcentaje2 = this.renderer.createText('%');
    const divPorcentaje2 = this.renderer.createElement('ion-col');
    this.renderer.addClass(divPorcentaje2, "Si");
    this.renderer.setAttribute(divPorcentaje2, "size", "1");
    this.renderer.appendChild(divPorcentaje2,porcentaje2);
    //end humendad
    //Viento
    const windSpeed = this.renderer.createElement('ion-col');
    const valueWind = this.renderer.createText((this.pronostico.currently.windSpeed) + '');

    this.renderer.addClass(windSpeed, "detalles");
    this.renderer.setAttribute(windSpeed, "size", "2");
    this.renderer.appendChild(windSpeed, valueWind);

    const iconWind= this.renderer.createElement('ion-col');
    const ruteWind = this.renderer.createElement('ion-img');

    this.renderer.setAttribute(iconWind, "size", "1");
    this.renderer.setAttribute(ruteWind, "src", "assets/icon/detalles/wind.png");
    this.renderer.addClass(ruteWind, "iconDetails");
    this.renderer.appendChild(iconWind, ruteWind);

    const ms = this.renderer.createText('m/s');
    const divms = this.renderer.createElement('ion-col');
    this.renderer.addClass(divms, "Si");
    this.renderer.setAttribute(divms, "size", "1");
    this.renderer.appendChild(divms,ms);
    //end viento

    //Asignacion de contenido
    this.renderer.appendChild(row, time);
    this.renderer.appendChild(row, icon);

    this.renderer.appendChild(row, humidity);
    this.renderer.appendChild(row, divPorcentaje);
    this.renderer.appendChild(row, iconHumidity);
    
    this.renderer.appendChild(row, precipProbability);
    this.renderer.appendChild(row, divPorcentaje2);
    this.renderer.appendChild(row, iconPrecip);

    this.renderer.appendChild(row, windSpeed);
    this.renderer.appendChild(row, divms);
    this.renderer.appendChild(row, iconWind);
    
  }

  parseTemperature(temperature) {
    return Math.floor(temperature);
  }

  parseDate(date) {
    return new Date(date * 1000);
  }

  selectTemperatureRange(temperature) {
    switch (true) {
      case (temperature >= 51):
      return 0;
      break;
      case (temperature >= 45 && temperature < 51):
      return 1;
      break;
      case (temperature >= 39 && temperature < 45):
      return 2;
      break;
      case (temperature >= 33 && temperature < 39):
      return 3;
      break;
      case (temperature >= 27 && temperature < 33):
      return 4;
      break;
      case (temperature >= 21 && temperature < 27):
      return 5;
      break;
      case (temperature >= 15 && temperature < 21):
      return 6;
      break;
      case (temperature >= 9 && temperature < 15):
      return 7;
      break;
      case (temperature >= 3 && temperature < 9):
      return 8;
      break;
      case (temperature >= -3 && temperature < 3):
      return 9;
      break;
      case (temperature >= -9 && temperature < -3):
      return 10;
      break;
      case (temperature >= -15 && temperature < -9):
      return 11;
      break;
      case (temperature >= -21 && temperature < -15):
      return 12;
      break;
      case (temperature >= -27 && temperature < -21):
      return 13;
      break;
      case (temperature >= -33 && temperature < -27):
      return 14;
      break;
      case (temperature >= -39 && temperature < -33):
      return 15;
      break;
      case (temperature >= -45 && temperature < -39):
      return 16;
      break;
      case (temperature < -45):
      return 17;
      break;
    }
  }


  slideController(){
    this.slides.isBeginning().then(isBeginning =>{this.slides.lockSwipeToPrev(isBeginning)});
    this.slides.isEnd().then(isEnd =>{this.slides.lockSwipeToNext(isEnd)});
  }

}
