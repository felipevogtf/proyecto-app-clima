import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiclimaService {

  constructor(public http: HttpClient) {
      console.log('Consultando datos de la api...');
   }

   getForecast(latitude, longitude){
    return this.http.get('https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/a3d2e2f0438c5ee6d4bcf19338ae91b2/'+latitude+','+longitude+'?units=si')
   }
}
