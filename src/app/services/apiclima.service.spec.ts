import { TestBed } from '@angular/core/testing';

import { ApiclimaService } from './apiclima.service';

describe('ApiclimaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiclimaService = TestBed.get(ApiclimaService);
    expect(service).toBeTruthy();
  });
});
